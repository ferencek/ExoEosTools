import os
import sys
import time
import pickle
from optparse import OptionParser
from pwd import getpwuid



def analyze(path, directories, exclusion_list, atime_thresh, verbose):

    if verbose:
        print 'Entering', path

    if path in exclusion_list:
        return False

    entries = os.listdir(path)

    # check if the directory is empty and mark it as such
    if len(entries) == 0:
        directories[path] = [0, os.path.getsize(path)]
        return True

    # a flag that specifies whether the entire directory with all its files and subdirectories can be removed
    removeAll = True
    # a flag that specifies whether all files in the directory can be removed
    removeFiles = True

    # check if the directory contains one of the excluded directories
    for entry in exclusion_list:
        if entry.startswith(path):
            removeAll = False

    nFiles = 0
    filesSize = 0

    for entry in entries:
        fullpath = os.path.join(path,entry)
        if os.path.isdir(fullpath):
            if fullpath in exclusion_list:
                removeAll = False
                continue
            else:
                removeAllSubDir = analyze(fullpath, directories, exclusion_list, atime_thresh, verbose)
                removeAll = removeAll and removeAllSubDir
        else:
            nFiles += 1
            try:
                filesSize += ( os.lstat(fullpath).st_size if os.path.islink(fullpath) else os.path.getsize(fullpath) )
                # check whether the file was accessed in the last N days
                if ( ( os.lstat(fullpath).st_atime > atime_thresh ) if os.path.islink(fullpath) else ( os.stat(fullpath).st_atime > atime_thresh ) ):
                    removeFiles = False
            except:
                print 'Problem getting time stamp info for', fullpath
                removeFiles = False

    removeAll = removeAll and removeFiles

    if removeAll:
        for d in directories.keys():
            if d.startswith(path):
                filesSize += directories[d][1]
                directories.pop(d, None)
        directories[path] = [1, filesSize]
    elif removeFiles and nFiles > 0:
        directories[path] = [2, filesSize]

    return removeAll


def getOwnerInfo(path, infoFile, verbose, recursive=False):

    if verbose:
        print 'Entering', path

    owner = 'N/A'
    try:
        uid = ( os.lstat(path).st_uid if os.path.islink(path) else os.stat(path).st_uid )
        owner = str(uid)
        owner = getpwuid(uid).pw_name
    except:
        print 'Problem getting owner info for', path

    infoFile.write('%-15s %s\n'%(owner, path))

    entries = os.listdir(path)

    for entry in entries:
        fullpath = os.path.join(path,entry)
        if os.path.isdir(fullpath) and recursive:
            getOwnerInfo(fullpath, infoFile, verbose, recursive)
        else:
            owner = 'N/A'
            try:
                uid = ( os.lstat(fullpath).st_uid if os.path.islink(fullpath) else os.stat(fullpath).st_uid )
                owner = str(uid)
                owner = getpwuid(uid).pw_name
            except:
                print 'Problem getting owner info for', fullpath

            infoFile.write('%-15s %s\n'%(owner, fullpath))


def removeFiles(path, verbose):

    if verbose:
        print 'Entering', path

    entries = os.listdir(path)

    for entry in entries:
        fullpath = os.path.join(path,entry)
        if os.path.isdir(fullpath):
            continue
        else:
            cmd = 'rm %s'%fullpath
            if verbose:
                print cmd
            os.system(cmd)


def main():
    # usage description
    usage = "Usage: python %prog [options] \nExample: python %prog -p /eos/cms/store/group/phys_exotica/ -e exclusion_list.txt"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-p", "--path", dest="path",
                      help="Path to the directory to be analyzed (This parameter is mandatory)",
                      metavar="PATH")

    parser.add_option("-e", "--exclude", dest="exclude",
                      help="Text file containing a list of directories to be excluded from the analysis (This parameter is mandatory)",
                      metavar="EXCLUSION_LIST")

    parser.add_option("-c", "--cache", dest="cache",
                      help="Cache file storing analysis results (This parameter is optional and set to 'directories.pkl' by default)",
                      default="directories.pkl",
                      metavar="CACHE_FILE")

    parser.add_option("-n", "--ndays", dest="ndays",
                      help="Maximum number of days allowed without file access (This parameter is optional and set to 365 days by default)",
                      default="365",
                      metavar="N_DAYS")

    parser.add_option("-o", "--output", dest="output",
                      help="Output file (This parameter is optional and set to 'deletion_list.txt' by default)",
                      default="deletion_list.txt",
                      metavar="OUTPUT")

    parser.add_option("--owner", dest="owner",
                      help="Text file storing owner info for directories and files to be deleted (This parameter is optional and set to an empty string by default)",
                      default="",
                      metavar="OWNER_INFO_FILE")

    parser.add_option("--reanalyze", dest="reanalyze", action='store_true',
                      help="Force reanalysis of the specified path instead of reusing the previously cached results (This parameter is optional)",
                      default=False)

    parser.add_option("--rc", dest="rc", action='store_true',
                      help="Remove complete directories (This parameter is optional)",
                      default=False)

    parser.add_option("--rf", dest="rf", action='store_true',
                      help="Remove files only (This parameter is optional)",
                      default=False)

    parser.add_option("--re", dest="re", action='store_true',
                      help="Remove empty directories (This parameter is optional)",
                      default=False)

    parser.add_option("--rm", dest="rm", action='store_true',
                      help="Remove everything (This parameter is optional)",
                      default=False)

    parser.add_option("-v", "--verbose", dest="verbose", action='store_true',
                      help="Verbose mode switch",
                      default=False)

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.path and options.exclude):
        print 'Mandatory parameters missing'
        print ''
        parser.print_help()
        sys.exit(1)

    # access time threshold
    atime_thresh = (time.time() - float(options.ndays) * 24 * 60 * 60)

    path = options.path
    path = path.rstrip('/')

    exclusion_list = file(options.exclude).readlines()
    tmp = []
    # the archival subdirectory is excluded by default
    tmp.append('/eos/cms/store/group/phys_exotica/archival')
    for d in exclusion_list:
        if d.lstrip().startswith('#'): continue
        line = d.lstrip().rstrip('/\n')
        if line == '': continue
        tmp.append(line)
    exclusion_list = tmp

    directories = {}
    shouldRebuild = options.reanalyze
    if os.path.exists(options.cache) and os.path.getsize(options.cache) == 0:
        print "Analysis results cache is empty and needs to be rebuilt"
        shouldRebuild = True
    shouldUpdate = False

    if os.path.exists(options.cache) and not shouldRebuild:
        with open(options.cache, 'rb') as fpkl:
            (p, directories) = pickle.load(fpkl)
            # ok to reuse the analysis results cache if it corresponds
            # to the same path or a parent directory of the specified path
            if p == path or path.startswith(p):
                print 'Analysis results cache loaded'
                # in the parent directory case need to collapse the deletion list to the specified subdirectory path
                if path.startswith(p):
                    for d in directories.keys():
                        if not d.startswith(path):
                            directories.pop(d, None)
                            shouldUpdate = True
                # check if there were any updates in the exclusion list since the analysis results cache was produced
                # first, check if any cached directories are subdirectories of the excluded directories. if so, remove them
                for d in directories.keys():
                    for entry in exclusion_list:
                        if d.startswith(entry):
                            directories.pop(d, None)
                            shouldUpdate = True
                # next, check if any directories marked for complete removal are parent directories of the excluded directories
                for d in directories.keys():
                    if directories[d][0] != 1: continue
                    for entry in exclusion_list:
                        if entry.startswith(d):
                            directories.pop(d, None)
                            analyze(d, directories, exclusion_list, atime_thresh, options.verbose)
                            shouldUpdate = True
            else:
                shouldRebuild = True
                print 'Analysis results cache corresponds to a different path or a subdirectory of the specified path and needs to be rebuilt'
                directories.clear()
    else:
        if shouldRebuild:
            print 'Analysis results cache will be rebuilt now'
        else:
            print 'Analysis results cache does not exist. Will be built now'

    # if there were any updates in the exclusion list, save the updated analysis results cache
    if shouldUpdate:
        print 'Updated analysis results cache saved'
        with open(options.cache, 'wb') as fpkl:
            dump = (path, directories)
            pickle.dump(dump, fpkl)

    if not os.path.exists(options.cache) or shouldRebuild:
        print 'Analyzing file system...'
        with open(options.cache, 'wb') as fpkl:
            analyze(path, directories, exclusion_list, atime_thresh, options.verbose)
            dump = (path, directories)
            pickle.dump(dump, fpkl)
        print 'Analysis results cache built and saved'

    empty = []
    completeDir = []
    filesOnly = []

    for d in sorted(directories.keys()):
        if directories[d][0] == 0:
            empty.append( d )
        elif directories[d][0] == 1:
            completeDir.append( d )
        elif directories[d][0] == 2:
            filesOnly.append( d )

    if len(directories.keys()):
        print '\nDirectories to be deleted being written into ' + options.output + '...'
        outputFile = open(options.output,'w')
        if len(completeDir):
            totalSize = 0
            for d in completeDir:
                totalSize += directories[d][1]
            outputFile.write('#---------------------------------------------\n')
            outputFile.write('# Complete directories: %.3f GB\n'%float(totalSize/(1024.0**3)))
            outputFile.write('#---------------------------------------------\n')
            for d in completeDir:
                outputFile.write('%s\n'%d)
            outputFile.write('\n')
        if len(filesOnly):
            totalSize = 0
            for d in filesOnly:
                totalSize += directories[d][1]
            outputFile.write('#---------------------------------------------\n')
            outputFile.write('# Contained files only: %.3f GB\n'%float(totalSize/(1024.0**3)))
            outputFile.write('#---------------------------------------------\n')
            for d in filesOnly:
                outputFile.write('%s\n'%d)
            outputFile.write('\n')
        if len(empty):
            totalSize = 0
            for d in empty:
                totalSize += directories[d][1]
            outputFile.write('#---------------------------------------------\n')
            outputFile.write('# Empty directories: %.3f GB\n'%float(totalSize/(1024.0**3)))
            outputFile.write('#---------------------------------------------\n')
            for d in empty:
                outputFile.write('%s\n'%d)
        outputFile.close()
        print 'Writing done\n'


        if len(directories.keys()) and options.owner != '':
            print 'Collecting file and directory owner info...'
            ownerInfoFile = open(options.owner,'w')
            for d in completeDir:
                getOwnerInfo(d, ownerInfoFile, options.verbose, recursive=True)
            for d in filesOnly:
                getOwnerInfo(d, ownerInfoFile, options.verbose, recursive=False)
            for d in empty:
                getOwnerInfo(d, ownerInfoFile, options.verbose, recursive=False)
            ownerInfoFile.close()
            print 'Owner info saved into ' + options.owner + '\n'


        if options.rc or options.rm:
            print 'Removing complete directories...'
            for d in completeDir:
                cmd = 'rm -rf %s'%d
                if options.verbose:
                    print cmd
                os.system(cmd)
            print 'Done with removing complete directories\n'

        if options.rf or options.rm:
            print 'Removing contained files only...'
            for d in filesOnly:
                removeFiles(d, options.verbose)
            print 'Done with removing contained files\n'

        if options.re or options.rm:
            print 'Removing empty directories...'
            for d in empty:
                cmd = 'rm -rf %s'%d
                if options.verbose:
                    print cmd
                os.system(cmd)
            print 'Done with removing empty directories\n'
    else:
        print '\nNo directories identified for deletion\n'



if __name__ == '__main__':
  main()
