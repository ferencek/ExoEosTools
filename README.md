# EXO EOS Tools

A set of Python scripts for managing files and directories stored on EOS.

## Table of contents

* [Software setup](#software-setup)
* [Analyzing file system](#analyzing-file-system)
   * [Caching of the analysis results](#caching-of-the-analysis-results)
   * [Saving the file owner information](#saving-the-file-owner-information)
* [Deleting files](#deleting-files)
* [Restoring deleted files](#restoring-deleted-files)

## Software setup

To use the EXO EOS tools, check them out anywhere on LXPLUS

```
git clone --depth 1 https://gitlab.cern.ch/ferencek/ExoEosTools.git

cd ExoEosTools
```

## Analyzing file system

The main script in the repository is `analyzeFileSystem.py`. For instructions on how to run it and to see available command-line options, run

```
python analyzeFileSystem.py -h
```
which will return

```
Usage: python analyzeFileSystem.py [options]
Example: python analyzeFileSystem.py -p /eos/cms/store/group/phys_exotica/ -e exclusion_list.txt

Options:
  -h, --help            show this help message and exit
  -p PATH, --path=PATH  Path to the directory to be analyzed (This parameter
                        is mandatory)
  -e EXCLUSION_LIST, --exclude=EXCLUSION_LIST
                        Text file containing a list of directories to be
                        excluded from the analysis (This parameter is
                        mandatory)
  -c CACHE_FILE, --cache=CACHE_FILE
                        Cache file storing analysis results (This parameter is
                        optional and set to 'directories.pkl' by default)
  -n N_DAYS, --ndays=N_DAYS
                        Maximum number of days allowed without file access
                        (This parameter is optional and set to 365 days by
                        default)
  -o OUTPUT, --output=OUTPUT
                        Output file (This parameter is optional and set to
                        'deletion_list.txt' by default)
  --owner=OWNER_INFO_FILE
                        Text file storing owner info for directories and files
                        to be deleted (This parameter is optional and set to
                        an empty string by default)
  --reanalyze           Force reanalysis of the specified path instead of
                        reusing the previously cached results (This parameter
                        is optional)
  --rc                  Remove complete directories (This parameter is
                        optional)
  --rf                  Remove files only (This parameter is optional)
  --re                  Remove empty directories (This parameter is optional)
  --rm                  Remove everything (This parameter is optional)
  -v, --verbose         Verbose mode switch
```

In a typical use case, one would run
```
python analyzeFileSystem.py -p /eos/cms/store/group/phys_exotica/ -e exclusion_list.txt -v
```
which would analyzes the entire EXO EOS area and check the most recent access time for all the files stored in that area. Please note
the `-v` (`--verbose`) option which turns on additional useful printout to the screen. In order to both see the printout on the screen
and have it saved to a text file for later inspection, just append the command with `| tee output.log`. In the analysis
all directories listed in `exclusion_list.txt` are ignored. The analysis results are summarized in the form of three categories of
candidate directories for deletion

1) **Complete directories**: none of the contained files and subdirectories 
have been accessed in the last 365 days

2) **Contained files only**: none of the contained files have been 
accessed in the last 365 days but files in some of the subdirectories have

3) **Empty directories**

The number of days is a configurable parameter and can be controlled by the `-n` (`--ndays`) option. The list candidate directories for deletion
is stored in an output file called `deletion_list.txt`. The name of the output file can changed using the `-o` (`--output`) option. An example of
the output file could look something like
```
#---------------------------------------------
# Complete directories: 3695.932 GB
#---------------------------------------------
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_MC/QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_MC/RSGravitonToQuarkQuark_kMpl01_Spring16_20161214_115751
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_cert/moriond16_v1_36fb/moriond16_v1_36fb_20161122_145923/merged
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_cert/moriond16_v1_36fb_23SeptJEC/moriond16_v1_36fb_23SeptJEC_20161211_210603/merged
...

#---------------------------------------------
# Contained files only: 511.389 GB
#---------------------------------------------
/eos/cms/store/group/phys_exotica/dijet
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/BstarToJJ_2016/1000
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/BstarToJJ_2016/2000
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/BstarToJJ_2016/3000
...

#---------------------------------------------
# Empty directories: 0.000 GB
#---------------------------------------------
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_cert/2016EF_20161108_173007
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_cert/fall2016_20160831_184446
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_cert/fall2016_20160831_184556
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/eos
...

```
Please note that the deletion list also includes the total size of files and directories selected for deletion for each of the three categories.

### Caching of the analysis results

Please note that running the file system analysis can take some time to complete. Because of that, the analysis results, in addition to being written
to the output file, are also cached in a local file called `directories.pkl`. The name of the cache is configurable and can be controlled by the
`-c` (`--cache`) option. Hence, if the same analysis command is repeated, for all subsequent runs there will be no real analysis performed and the
output file will be produced very quickly. This prevents any unintentional or accidental loss of the previous analysis results. In case a reanalysis
is needed, it is sufficient to either delete the cache file or add the `--reanalyze` option.

Another advantage of the cache file is the analysis of a subdirectory of the previously analyzed path. In such cases, again, no real analysis will be performed
and the old cached results will be reused and restricted to the specified subdirectory. Similarly, if a directory is added to the exclusion list, the old
cached results will be reused or, if needed, a partial reanalysis will be performed.

Finally, in a typical use case, one would first analyze the file system and after looking at the list of candidate directories for deletion, proceed with
deletion. This implies running the analysis command with extra command-line options (see below) but thanks to the cache file, there is no need to reanalyze
the entire file system and the deletion can start right away.

### Saving the file owner information

Because of the way the [EOS recycle bin](https://twiki.cern.ch/twiki/bin/view/EOS/RecoverDeletedData) works, where deleted files need to be restored by their owners, it is generally recommended to store the owner
info before deleting any directories. This can be done by adding the `--owner` option to the analysis command, e.g., `--owner=owner_info.txt`. An example
of the owner info could look something like
```
deguio          /eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_MC/QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938
deguio          /eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_MC/QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938/cutEfficiencyFile_QstarToJJ_M_1000_TuneCUETP8M1_13TeV_pythia8_QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938_0.dat
deguio          /eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_MC/QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938/cutEfficiencyFile_QstarToJJ_M_2000_TuneCUETP8M1_13TeV_pythia8_QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938_0.dat
deguio          /eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/deguio/fall16_red_MC/QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938/cutEfficiencyFile_QstarToJJ_M_3000_TuneCUETP8M1_13TeV_pythia8_QstarToJJ_TuneCUETP8M1_13TeV_pythia8_20170115_213938_0.dat
...
```

## Deleting files

After the file system analysis has been completed, each of the directory categories can be deleted separately (options `--rc`, `--rf` and `--re`) or all at
once (option `--rm`). So a typical use case would be to run the file systenm analysis first
```
python analyzeFileSystem.py -p /eos/cms/store/group/phys_exotica/ -e exclusion_list.txt --owner=owner_info.txt -v
```
followed by file and directory deletion
```
python analyzeFileSystem.py -p /eos/cms/store/group/phys_exotica/ -e exclusion_list.txt -v --rm
```

## Restoring deleted files

File owners can restore their files from the [EOS recycle bin](https://twiki.cern.ch/twiki/bin/view/EOS/RecoverDeletedData) in case they were accidentally or
for any other reason deleted but are still needed. The first step in the file restoration is to obtain the list of recently deleted files
```
eos recycle ls > restore_list.txt
```
To restore the files, the `restoreFiles.py` script can be used. The script can be used to restore files deleted on a specific date, as shown in the script help
```
python restoreFiles.py -h

Usage: python restoreFiles.py [options]
Example: python restoreFiles.py -l restore_list.txt -y 2018 -m Aug -d 29

Options:
  -h, --help            show this help message and exit
  -l LIST, --list=LIST  Location of the restore list file (This parameter is
                        mandatory)
  -y YEAR, --year=YEAR  Deletion year (This parameter is mandatory)
  -m MONTH, --month=MONTH
                        Deletion month (This parameter is mandatory)
  -d DAY, --day=DAY     Deletion day (This parameter is mandatory)
```

