import os
import sys
from optparse import OptionParser


def main():
    # usage description
    usage = "Usage: python %prog [options] \nExample: python %prog -l restore_list.txt -y 2018 -m Aug -d 29"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-l", "--list", dest="restore_list",
                        help="Location of the restore list file (This parameter is mandatory)",
                        metavar="LIST")

    parser.add_option("-y", "--year", dest="year",
                        help="Deletion year (This parameter is mandatory)",
                        metavar="YEAR")

    parser.add_option("-m", "--month", dest="month",
                        help="Deletion month (This parameter is mandatory)",
                        metavar="MONTH")

    parser.add_option("-d", "--day", dest="day",
                        help="Deletion day (This parameter is mandatory)",
                        metavar="DAY")

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.restore_list and options.year and options.month and options.day):
        parser.print_help()
        sys.exit(1)

    counter = 0

    for line in open(options.restore_list,"r"):
        if line.startswith("#"): continue
        if not (line.split()[4] == options.year and line.split()[1] == options.month and line.split()[2] == options.day): continue
        key = line.split()[9]
        dirname = os.path.dirname(os.path.abspath(line.split()[10]))
        print "Restoring " + line.split()[10]
        if not os.path.exists(dirname):
            os.system("mkdir -p " + dirname)
        os.system("eos recycle restore " + key)
        counter += 1

    print counter, "file(s) restored"


if __name__ == '__main__':
    main()
