# Please note that the archival subdirectory
# /eos/cms/store/group/phys_exotica/archival/
# is excluded by default and does not need to be explicitly listed here

/eos/cms/store/group/phys_exotica/bffZprime/
/eos/cms/store/group/phys_exotica/bbMET/
/eos/cms/store/group/phys_exotica/blackholeFull2016/
/eos/cms/store/group/phys_exotica/darkPhoton/
/eos/cms/store/group/phys_exotica/dijet/dazsle/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/DijetMass_spectrum/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/dimitris/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/eirini/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/magda/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/niki/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/reducedNtuples/mc/Spring16_25ns_JEC_Spring16_25nsV3/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/TylerW/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeVScouting/rootTrees_big/MC/background/Spring15_25ns_JEC_Summer15_25nsV7/
/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV_WideResonanceSamples/
/eos/cms/store/group/phys_exotica/dimuon/
/eos/cms/store/group/phys_exotica/diphoton/
/eos/cms/store/group/phys_exotica/disappearingTracks/
/eos/cms/store/group/phys_exotica/displacedPhotons/
/eos/cms/store/group/phys_exotica/HNL/
/eos/cms/store/group/phys_exotica/hscp/
/eos/cms/store/group/phys_exotica/leptonsPlusJets/leptoquarks/
/eos/cms/store/group/phys_exotica/leptonsPlusJets/RootNtuple/RunII/dmorse/
/eos/cms/store/group/phys_exotica/leptonsPlusJets/RootNtuple/RunII/scooper/
/eos/cms/store/group/phys_exotica/leptonsPlusJets/RootNtuple/RunII/vinguyen/
/eos/cms/store/group/phys_exotica/leptonsPlusJets/RootNtuple_skim/2016/
/eos/cms/store/group/phys_exotica/leptonsPlusJets/WR/
/eos/cms/store/group/phys_exotica/MonoHgg/
/eos/cms/store/group/phys_exotica/monojet/
/eos/cms/store/group/phys_exotica/monopole/
/eos/cms/store/group/phys_exotica/Monopole_2017/
/eos/cms/store/group/phys_exotica/monoZ/
/eos/cms/store/group/phys_exotica/QBH_LHE_ADD/
/eos/cms/store/group/phys_exotica/QBH_LHE_RS1/
/eos/cms/store/group/phys_exotica/trijetResonances/
